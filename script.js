function piu(id){
    var counter= document.getElementById("counter"+id);
    var previous= document.getElementById("previous"+id);
    var step=document.getElementById("step"+id);
    var prev=parseInt(counter.innerHTML);
    var add;
    if(step.value=="")
        add=1;
    else
        add=parseInt(step.value);
    
    previous.innerHTML = prev;
    counter.innerHTML = prev+add;
    step.value=null;    
}

function meno(id){
    var counter= document.getElementById("counter"+id);
    var previous= document.getElementById("previous"+id);
    var step=document.getElementById("step"+id);
    var prev=parseInt(counter.innerHTML);
    if(step.value=="")
        add=1;
    else
        add=parseInt(step.value);
    
    previous.innerHTML = prev;  
    counter.innerHTML = prev-add; 
    step.value=null;
}

function reset(id){
    if (!confirm("vuoi davvero resettare!?"))
        return false;
    var counter= document.getElementById("counter"+id);
    var previous= document.getElementById("previous"+id);
    var step=document.getElementById("step"+id);
    var prev=parseInt(counter.innerHTML);
    previous.innerHTML = prev;  
    counter.innerHTML = 0;    
    step.value=null;
}

function undo(id){
    if (!confirm("vuoi davvero annullare!?"))
        return false;
    var counter= document.getElementById("counter"+id);
    var previous= document.getElementById("previous"+id);
    var step=document.getElementById("step"+id); 
    var prev=parseInt(counter.innerHTML);
    var undo=parseInt(previous.innerHTML);
    counter.innerHTML = undo;    
    previous.innerHTML = prev;  
    step.value=null;
}

function checkkey(event,id) {
    var c=event.which;
    var d=event.keyCode;
    if (c == 13) {
        piu(id);
        return true;
    }else if(48<=c && c<=57){//only digits
        return true;
    }else if(c==8 || c==45 || d==37 || d==39 ){//delete - minus
        return true;
    }
    return false;
}
    
function digit(id,digit){
    var step= document.getElementById("step"+id);
    var prec=step.value;
    step.value=prec+digit;
    return true;
}
